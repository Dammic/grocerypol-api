import { ProductType, ShoppingListType } from './types/product'

export const productsMock: ProductType[] = [
  {
    name: 'Egg',
    checked: false,
    id: '1',
  },
  {
    name: 'Serek wiejski',
    checked: false,
    id: '2',
  },
  {
    name: 'Maslo',
    checked: true,
    id: '3',
  },
]

export let shoppingListsMock: ShoppingListType[] = [
  {
    id: '1',
    name: 'my shopping list',
    date: '30.01.11',
    authorId: '1',
    items: productsMock
  },
  {
    id: '2',
    name: 'my shopping list 2',
    date: '30.01.11',
    authorId: '1',
    items: productsMock
  },
]

export let data = {
  shoppingListsMock
}
