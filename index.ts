import * as Koa from 'koa'
import * as logger from 'koa-morgan'
import * as Router from 'koa-router'
import * as graphqlHTTP from 'koa-graphql'
import * as cors from '@koa/cors'
import { buildSchema } from 'graphql'
import { getShoppingLists } from './databaseActions'
import { data } from './mockedData'
import { generateId } from './utils'

const schema = buildSchema(`
  type Query {
    products: [ProductType]
    shoppingLists: [ShoppingListType]
    shoppingList(shoppingListId: ID!): ShoppingListType
  }
  type Mutation {
    updateItem(itemId: ID!, shoppingListId: ID!, checked: Boolean, newName: String): ShoppingListType
    addItem(shoppingListId: ID!, name: String): ShoppingListType
    removeItem(itemId: ID!, shoppingListId: ID!): ShoppingListType
    updateShoppingList(shoppingListId: ID!, name: String): ShoppingListType
    removeShoppingList(shoppingListId: ID!): [ShoppingListType]
    addShoppingList: ID
  }
  type ProductType {
    name: String,
    checked: Boolean,
    id: ID!
  }
  type ShoppingListType {
    id: ID!,
    name: String,
    date: String,
    authorId: ID!,
    items: [ProductType]
  }
`);

// Root resolver
const root = {
  shoppingLists: () => {
    getShoppingLists()
    return data.shoppingListsMock
  },
  shoppingList: ({ shoppingListId }: { shoppingListId: string }) => {
    return data.shoppingListsMock.find((list) => list.id === shoppingListId)
  },
  updateShoppingList: ({ shoppingListId, name }: { shoppingListId: string, name: string }) => {
    const shoppingList = data.shoppingListsMock.find((list) => list.id === shoppingListId)
    if (!shoppingList) {
      throw new Error()
    }
    shoppingList.name = name
    return shoppingList
  },
  removeShoppingList: ({ shoppingListId }: { shoppingListId: string}) => {
    const shoppingList = data.shoppingListsMock.find((list) => list.id === shoppingListId)
    if (!shoppingList) {
      throw new Error()
    }
    data.shoppingListsMock = data.shoppingListsMock.filter((list) => list.id !== shoppingListId)
    return data.shoppingListsMock
  },
  addShoppingList: () => {
    const newList = {
      id: generateId(),
      name: 'My shopping list',
      date: '30.01.11',
      authorId: '1',
      items: [],
    }
    data.shoppingListsMock.push(newList)
    return newList.id
  },
  updateItem: ({ itemId, shoppingListId, checked, newName }) => {
    const shoppingList = data.shoppingListsMock.find((list) => list.id === shoppingListId)
    if (!shoppingList) {
      throw new Error()
    }

    const product = shoppingList.items.find((item) => item.id === itemId)
    if (!product) {
      throw new Error()
    }
    
    product.checked = checked
    product.name = newName
    return shoppingList
  },
  addItem: ({ shoppingListId, checked, name }) => {
    const shoppingList = data.shoppingListsMock.find((list) => list.id === shoppingListId)
    if (!shoppingList) {
      throw new Error()
    }
    const newProduct = {
        id: generateId(),
        checked: false,
        name,
    }
    shoppingList.items.push(newProduct)

    return shoppingList
  },
  removeItem: ({ shoppingListId, itemId }) => {
    const shoppingList = data.shoppingListsMock.find((list) => list.id === shoppingListId)
    if (!shoppingList) {
      throw new Error() 
    }
    shoppingList.items = shoppingList.items.filter((item) => item.id !== itemId)
    return shoppingList
  },
};

const router = new Router()
const server = new Koa()

router.all('/graphql', graphqlHTTP({
  schema,
  rootValue: root,
  graphiql: true
}));

server
  .use(cors({
    origin: "*" 
  }))
  .use(logger('tiny'))
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(3000, () => {
    console.log(`Server started and running on localhost:3000`)
  })

server.on('error', (err, ctx) => {
  console.error(err, ctx)
})
