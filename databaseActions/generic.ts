import { MongoClient } from 'mongodb'

const url = 'mongodb://grocerypol_mongodb:27017'
const dbName = 'grocerypol'

export const connectToDatabase = async () => {
  try {
    const client = await MongoClient.connect(url, { useNewUrlParser: true })
    const db = client.db(dbName)
    return { client, db }
  } catch (e) {
    console.error(e)
  }
}

