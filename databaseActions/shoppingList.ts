import { connectToDatabase } from './generic'

export const getShoppingLists = async () => (
  new Promise(async (resolve, reject) => {
    const { client, db } = await connectToDatabase()
    try {
      console.log('yay!')
    } catch (e) {
      console.error(e)
    } finally {
      if (client) {
        client.close()
      }
    }
  })
)
